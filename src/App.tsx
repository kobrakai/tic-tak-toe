import React from 'react'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Normalize } from 'styled-normalize'
import Theme, { globalCss } from 'theme'
import Game from 'components/Game'

const GlobalStyles = createGlobalStyle`${globalCss}`

const App: React.FC = () => (
	<ThemeProvider theme={Theme}>
		<Normalize />
		<GlobalStyles />
		<Game />
	</ThemeProvider>
)

export default App
