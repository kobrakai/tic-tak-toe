export interface ColorsProps {
	[key: string]: ColorList
}

export interface ColorList {
	[key: string]: string
}

export const Colors: ColorsProps = {
	primary: {
		dark: '#ff8c42',
		main: '#fcaf58',
		light: '#f9Cf84',
		text: '#523023'
	},
	secondary: {
		dark: '#26a195',
		main: '#2ec4b6',
		light: '#7ad9d0',
		text: '#1f2d3d'
	}
}
