import { HTMLAttributes } from 'react'
import styled, { css, keyframes } from 'styled-components'

interface WrapperProps extends HTMLAttributes<HTMLButtonElement> {
	highlighted?: boolean
	winner?: number
}

const pulsating = keyframes`
	0% {
		font-size: 8rem;
	}

	100% {
		font-size: 5rem;
    }
`

export const Wrapper = styled.button<WrapperProps>`
	--border-width: 4px;
	width: 10rem;
	height: 10rem;
	border-width: 0 var(--border-width) var(--border-width) 0;
	border-style: solid;
	border-color: ${({ theme }) => theme.colors.primary.text};
	background-color: transparent;
	font-size: 5rem;

	&:disabled {
		color: black;
	}

	&:not(:empty) {
		animation: ${pulsating} 0.1s linear;
	}

	${({ theme, highlighted, winner }) =>
		highlighted
			? css`
					background-color: ${winner === 1
						? theme.colors.primary.light
						: theme.colors.secondary.light};
			  `
			: ''}

	&:nth-child(3n) {
		border-right-width: 0;
	}

	&:nth-child(1n + 7) {
		border-bottom-width: 0;
	}
`
