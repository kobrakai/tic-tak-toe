import React, { useContext } from 'react'
import GameContext from 'data/GameContext'
import * as Styles from './styles'

export interface BoxProps {
	index: number
	highlighted?: number
	children?: string
}

const Box: React.FC<BoxProps> = ({
	index,
	highlighted,
	children,
	...rest
}: BoxProps) => {
	const { board, setBoard, player, winner, isDraw } = useContext(GameContext)

	const onClick = () => {
		const newBoard = [...board]
		newBoard[index] = player === 1 ? 'X' : 'O'
		setBoard(newBoard)
	}

	return (
		<Styles.Wrapper
			onClick={onClick}
			highlighted={highlighted !== undefined}
			winner={winner}
			disabled={winner || isDraw || children ? true : false}
			{...rest}
		>
			{children}
		</Styles.Wrapper>
	)
}

export default Box
