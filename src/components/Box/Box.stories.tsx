import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Box from './Box'

export default {
	title: 'Components/Box',
	component: Box
} as Meta

export const Default = () => <Box index={1} />

Default.parameters = {
	title: 'Box',
	subtitle: ''
}
