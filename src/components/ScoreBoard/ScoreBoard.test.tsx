import React from 'react'
import renderer from 'react-test-renderer'
import ScoreBoard from './ScoreBoard'
  
describe('ScoreBoard', () => {
	it("renders correctly", () => {
		const tree = renderer.create(<ScoreBoard />).toJSON();
		expect(tree).toMatchSnapshot();
	});
});
    