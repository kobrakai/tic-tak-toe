import { HTMLAttributes } from 'react'
import styled, { css, keyframes } from 'styled-components'

interface WrapperProps extends HTMLAttributes<HTMLDivElement> {
	winner?: number
}

interface PlayerProps extends HTMLAttributes<HTMLDivElement> {
	active: boolean
}

const pulsating = keyframes`
	0% {
		transform: scale(1.8);
	}

	33.333% {
		transform: scale(1);
	}

	66.666% {
		transform: scale(1.8);
    }
    
    100% {
		transform: scale(1);
	}
`

export const Wrapper = styled.div<WrapperProps>`
	position: relative;
	display: flex;
	justify-content: space-between;
	margin: 2rem auto 0;
	max-width: ${(props) => props.theme.grid.xs};
	padding: 0.8rem;
	overflow: hidden;
	background-color: ${({ theme }) => theme.colors.primary.main};

	&::after {
		content: '';
		width: 50%;
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		background-color: ${({ theme }) => theme.colors.secondary.main};
		transition: width 0.3s ease-out;
		z-index: 0;
	}

	${({ winner }) =>
		winner === 1 &&
		css`
			&::after {
				width: 0%;
			}
		`}

	${({ winner }) =>
		winner === 2 &&
		css`
			&::after {
				width: 100%;
			}
		`}
`

export const Player = styled.div<PlayerProps>`
	opacity: 0.4;
	z-index: 1;

	&:last-child {
		text-align: right;
	}

	${({ active }) =>
		active
			? css`
					opacity: 1;
			  `
			: ''}
`

export const PlayerName = styled.div<PlayerProps>`
	${({ active }) =>
		active
			? css`
					animation: ${pulsating} 0.3s linear 0.3s;
			  `
			: ''}
`
