import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import ScoreBoard from './ScoreBoard'

export default {
	title: 'Components/ScoreBoard',
	component: ScoreBoard
} as Meta

export const Default = () => <ScoreBoard />

Default.parameters = {
	title: 'ScoreBoard',
	subtitle: ''
}
      