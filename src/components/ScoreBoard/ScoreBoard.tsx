import React, { useContext } from 'react'
import GameContext from 'data/GameContext'
import * as Styles from './styles'

interface ScoreBoardProps {}

const ScoreBoard: React.FC<ScoreBoardProps> = ({
	...rest
}: ScoreBoardProps) => {
	const { player, winner, playerOneScore, playerTwoScore } = useContext(
		GameContext
	)

	return (
		<Styles.Wrapper winner={winner} {...rest}>
			<Styles.Player active={player === 1}>
				<Styles.PlayerName active={player === 1}>P1</Styles.PlayerName>
				{playerOneScore} wins
			</Styles.Player>
			<Styles.Player active={player === 2}>
				<Styles.PlayerName active={player === 2}>P2</Styles.PlayerName>
				{playerTwoScore} wins
			</Styles.Player>
		</Styles.Wrapper>
	)
}

export default ScoreBoard
