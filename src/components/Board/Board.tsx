import React, { useContext } from 'react'
import Box from 'components/Box'
import GameContext from 'data/GameContext'
import * as Styles from './styles'

export interface BoardProps {
	winningCombo?: number[]
}

const Board: React.FC<BoardProps> = ({ winningCombo, ...rest }: BoardProps) => {
	const { board, winner, isDraw } = useContext(GameContext)

	return (
		<Styles.Wrapper {...rest}>
			{board.map((value, key) => (
				<Box
					key={key}
					index={key}
					highlighted={
						winningCombo &&
						winningCombo.find((number) => number === key)
					}
				>
					{value}
				</Box>
			))}

			<Styles.Overlay winner={winner} isDraw={isDraw}>
				{winner ? <Styles.Text>Player {winner} wins!</Styles.Text> : ''}
				{isDraw ? <Styles.Text>It's a draw!</Styles.Text> : ''}
			</Styles.Overlay>
		</Styles.Wrapper>
	)
}

export default Board
