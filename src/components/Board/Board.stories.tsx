import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Board from './Board'

export default {
	title: 'Components/Board',
	component: Board
} as Meta

export const Default = () => <Board />

Default.parameters = {
	title: 'Board',
	subtitle: ''
}
