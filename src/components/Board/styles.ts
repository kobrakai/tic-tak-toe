import styled, { css, keyframes } from 'styled-components'
import { rgba } from 'polished'

interface OverlayProps {
	winner?: number
	isDraw: boolean
}

const pulsating = keyframes`
	0% {
		transform: scale(0.95);
		text-shadow: 0 0 0 rgba(0, 0, 0, 0);
	}

	70% {
		transform: scale(1);
		text-shadow: 0 0 18px rgba(0, 0, 0, 0.5);
	}

	100% {
		transform: scale(0.95);
		text-shadow: 0 0 0 rgba(0, 0, 0, 0);
	}
`

export const Wrapper = styled.div`
	position: relative;
	display: flex;
	margin: 0 auto;
	max-width: ${(props) => props.theme.grid.xs};
	flex-wrap: wrap;
`

export const Overlay = styled.div<OverlayProps>`
	display: flex;
	justify-content: center;
	align-items: center;
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	opacity: ${({ isDraw, winner }) => (isDraw || winner ? '1' : '0')};
	backdrop-filter: blur(10px);
	transition: all 0.5s ease-out;
	pointer-events: ${({ isDraw, winner }) =>
		isDraw || winner ? 'all' : 'none'};

	${({ winner, theme }) =>
		winner
			? css`
					background-color: ${winner === 1
						? rgba(theme.colors.primary.main, 0.2)
						: rgba(theme.colors.secondary.main, 0.2)};
			  `
			: css`
					background-color: rgba(0, 0, 0, 0.2);
			  `}
`

export const Text = styled.div`
	font-size: 2rem;
	animation: ${pulsating} 2s linear infinite;
`
