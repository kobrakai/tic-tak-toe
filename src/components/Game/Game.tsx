import React, { useState, useEffect } from 'react'
import Board from 'components/Board'
import ScoreBoard from 'components/ScoreBoard'
import Reset from 'components/Reset'
import { GameProvider } from 'data/GameContext'
import * as Styles from './styles'

interface GameProps {}

const Game: React.FC<GameProps> = ({ ...rest }: GameProps) => {
	const [board, setBoard] = useState<Array<string>>([...Array(9)])
	const [player, setPlayer] = useState<number>(1)
	const [winnningCombo, setWinningCombo] = useState<Array<number>>([])
	const [winner, setWinner] = useState<number | undefined>()
	const [isDraw, setIsDraw] = useState<boolean>(false)
	const [playerOneScore, setPlayerOneScore] = useState<number>(0)
	const [playerTwoScore, setPlayerTwoScore] = useState<number>(0)

	const changePlayer = (targetPlayer?: 1 | 2) => {
		const newPlayer = player === 1 ? 2 : 1

		if (targetPlayer) {
			setPlayer(targetPlayer)
		} else {
			setPlayer(newPlayer)
		}
	}

	const state = {
		board,
		setBoard,
		player,
		changePlayer,
		winner,
		setWinner,
		setWinningCombo,
		isDraw,
		setIsDraw,
		playerOneScore,
		playerTwoScore
	}

	useEffect(() => {
		let hasWinner = false
		const isFull = board.findIndex((number) => number === undefined) === -1
		const filled =
			board.findIndex((number) => number === 'X' || number === 'O') !== -1

		const combos = [
			[0, 1, 2],
			[3, 4, 5],
			[6, 7, 8],
			[0, 3, 6],
			[1, 4, 7],
			[2, 5, 8],
			[0, 4, 8],
			[2, 4, 6]
		]

		for (let index = 0; index < combos.length; index++) {
			const [a, b, c] = combos[index]

			if (board[a] && board[a] === board[b] && board[a] === board[c]) {
				const isPlayerOne = board[a] === 'X'
				hasWinner = true
				setWinningCombo(combos[index])
				setWinner(isPlayerOne ? 1 : 2)
			}
		}

		if (filled && !hasWinner) {
			changePlayer()
		}

		if (!hasWinner && isFull) {
			setIsDraw(true)
		}
		// eslint-disable-next-line
	}, [board])

	useEffect(() => {
		if (winner) {
			if (winner === 1) {
				setPlayerOneScore(playerOneScore + 1)
			} else {
				setPlayerTwoScore(playerTwoScore + 1)
			}
		}
		// eslint-disable-next-line
	}, [winner])

	return (
		<GameProvider value={state}>
			<Styles.Wrapper isDraw={isDraw} player={player} {...rest}>
				<Board winningCombo={winnningCombo} />
				<ScoreBoard />
				<Reset />
			</Styles.Wrapper>
		</GameProvider>
	)
}

export default Game
