import styled, { css } from 'styled-components'
import { rgba } from 'polished'

interface WrapperProps {
	player: number
	isDraw: boolean
}

export const Wrapper = styled.div<WrapperProps>`
	width: 100%;
	height: 100vh;
	padding-top: clamp(50px, 10%, 200px);
	transition: background 0.3s ease-out;

	${({ isDraw, player, theme }) =>
		isDraw
			? css`
					background-color: rgba(0, 0, 0, 0.2);
			  `
			: css`
					background-color: ${player === 1
						? rgba(theme.colors.primary.light, 0.8)
						: rgba(theme.colors.secondary.light, 0.8)};
			  `}
`
