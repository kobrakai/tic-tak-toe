import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Game from './Game'

export default {
	title: 'Components/Game',
	component: Game
} as Meta

export const Default = () => <Game />

Default.parameters = {
	title: 'Game',
	subtitle: ''
}
      