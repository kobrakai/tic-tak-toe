import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Reset from './Reset'

export default {
	title: 'Components/Reset',
	component: Reset
} as Meta

export const Default = () => <Reset />

Default.parameters = {
	title: 'Reset',
	subtitle: ''
}
      