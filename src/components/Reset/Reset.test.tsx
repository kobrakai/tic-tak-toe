import React from 'react'
import renderer from 'react-test-renderer'
import Reset from './Reset'
  
describe('Reset', () => {
	it("renders correctly", () => {
		const tree = renderer.create(<Reset />).toJSON();
		expect(tree).toMatchSnapshot();
	});
});
    