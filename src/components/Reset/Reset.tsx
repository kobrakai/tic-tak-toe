import React, { useContext } from 'react'
import GameContext from 'data/GameContext'
import * as Styles from './styles'

interface ResetProps {}

const Reset: React.FC<ResetProps> = ({ ...rest }: ResetProps) => {
	const {
		setBoard,
		winner,
		setWinner,
		setIsDraw,
		changePlayer,
		setWinningCombo
	} = useContext(GameContext)

	const handleClick = () => {
		setBoard([...Array(9)])
		setWinner(undefined)
		setWinningCombo([])
		setIsDraw(false)

		if (!winner) {
			changePlayer(1)
		} else {
			changePlayer(winner === 1 ? 2 : 1)
		}
	}

	return (
		<Styles.Wrapper onClick={handleClick} {...rest}>
			Reset
		</Styles.Wrapper>
	)
}

export default Reset
