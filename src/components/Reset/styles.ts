import styled from 'styled-components'

export const Wrapper = styled.button`
	display: block;
	margin: 1rem auto 0;
	padding: 0.35em 1.2em;
	border: 0.1em solid black;
	border-radius: 0.12em;
	box-sizing: border-box;
	text-decoration: none;
	color: black;
	text-align: center;
	transition: all 0.3s ease-out;
	background-color: transparent;

	&:hover {
		background-color: rgba(0, 0, 0, 0.1);
	}
`
