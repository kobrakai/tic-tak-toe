import React from 'react'

export interface GameContextProps {
	board: string[]
	setBoard: Function
	player: number
	changePlayer: Function
	winner?: number
	setWinner: Function
	setWinningCombo: Function
	isDraw: boolean
	setIsDraw: Function
	playerOneScore: number
	playerTwoScore: number
}

const GameContext = React.createContext<GameContextProps>({
	board: [...Array(9)],
	setBoard: () => {},
	player: 1,
	changePlayer: () => {},
	setWinner: () => {},
	setWinningCombo: () => {},
	isDraw: false,
	setIsDraw: () => {},
	playerOneScore: 0,
	playerTwoScore: 0
})

export const GameProvider = GameContext.Provider
export const GameConsumer = GameContext.Consumer

export default GameContext
