import React from 'react'
import styled, { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Normalize } from 'styled-normalize'
import Theme, { globalCss } from '../src/theme'

const GlobalStyles = createGlobalStyle`${globalCss}`
const Wrapper = styled.div`
	padding-top: 40px;
	padding-bottom: 40px;
`

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' }
}

export const decorators = [
	(Story) => (
		<ThemeProvider theme={Theme}>
			<Normalize />
			<GlobalStyles />
			<Wrapper>
				<Story />
			</Wrapper>
		</ThemeProvider>
	)
]
